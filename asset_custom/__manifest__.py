{
    'name': 'Asset Custom',
    'version': '1.0',
    'category': '',
    'website': '',
    'author' : '',
    'depends': ['mail','base', 'web', 'product', 'maintenance', 'account', 'purchase'],
    'data' : [
            'security/ir.model.access.csv',
            'views/product_template_views.xml',
            'views/maintenance_views.xml',
            'views/master_data_new_views.xml',
        ],
    'installable': 'True',
}
from odoo import api, fields, models

class WorkCenterMain(models.Model):
    _name = "work.center.maintenance"

    name = fields.Char("Work Center Name", required=True)

    def name_get(self):
        result = []
        for mt in self:
            result.append((mt.id, "%s" % (mt.name or '')))
        return result

class AreaMaintenance(models.Model):
    _name = "area.maintenance"

    name = fields.Char("Area Name", required=True)

    def name_get(self):
        result = []
        for mt in self:
            result.append((mt.id, "%s" % (mt.name or '')))
        return result

class SegmenMaintenance(models.Model):
    _name = "segmen.maintenance"

    name = fields.Char("Segmen Name", required=True)

    def name_get(self):
        result = []
        for mt in self:
            result.append((mt.id, "%s" % (mt.name or '')))
        return result

class SegmenMaintenance(models.Model):
    _name = "segmen.maintenance"

    name = fields.Char("Segmen Name", required=True)

    def name_get(self):
        result = []
        for mt in self:
            result.append((mt.id, "%s" % (mt.name or '')))
        return result
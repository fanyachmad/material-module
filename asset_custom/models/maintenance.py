from odoo import api, fields, models

class MaintenanceEquipment(models.Model):
    _inherit = 'maintenance.equipment'

    asset_code = fields.Char("Asset Code")
    no_aktiva = fields.Many2one('account.account', string="No Aktiva")
    id_component = fields.Char("ID Component")
    barcode = fields.Char("Barcode")
    status = fields.Selection([('active', 'Active'),('non_active', 'Non Active')], string="Active Status")
    scrap_date = fields.Date("Scrap Date")
    work_center_main = fields.Many2one('work.center.maintenance', string="Work Center")
    area_main = fields.Many2one('area.maintenance', string="Area")
    segmen = fields.Many2one('segmen.maintenance', string="Segmen")
    latitude = fields.Float("Latitude", digits=(16, 1))
    longitude = fields.Float("Longitude", digits=(16, 1)    )
    asset_breakdown_attc = fields.Binary("Asset Breakdown Attachment")
    asset_breakdown_attc_edit = fields.Binary("Asset Breakdown Attachment Edit")
    spk_number = fields.Many2one('purchase.order', string='SPK Number')
    as_built_drawing_attch = fields.Binary("As Built Drawing Attachment")
    bast_number = fields.Char("Bast Number")

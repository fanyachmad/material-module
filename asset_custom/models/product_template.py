from odoo import api, fields, models

class ProductTemplate(models.Model):
    _inherit = "product.template"
    _description = "Module Asset Management Custom"

    is_asset = fields.Boolean("Is Asset", default=True)
    asset_number = fields.Char("Asset Number")
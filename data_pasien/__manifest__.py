{
    'name': 'Data Pasien',
    'version': '1.0',
    'category': '',
    'website': '',
    'author' : 'Fany Achmad',
    'depends': ['mail','base', 'web'],
    'data' : [
            'security/ir.model.access.csv',
            'views/data_pasien_views.xml',
            'views/diagnosis_views.xml',
            'views/pemeriksaan_views.xml',
            'views/terapi_views.xml',
            'report/ir_actions_report.xml',
            'report/data_pasien_report.xml',
        ],
    'installable': 'True',
}
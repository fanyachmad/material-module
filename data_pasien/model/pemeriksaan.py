from odoo import api, fields, models

class Pemeriksaan(models.Model):
    _name = "pemeriksaan.pasien"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char("Nama Pasien")
    tgl_periksa = fields.Date("Tanggal Periksa")
    evaluasi = fields.Text("Evaluasi")
    keluhan = fields.Char("Keluhan Utama")

    def name_get(self):
        result = []
        for mt in self:
            result.append((mt.id, "%s" % (mt.name or '')))
        return result
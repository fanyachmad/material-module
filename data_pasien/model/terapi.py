from odoo import api, fields, models

class TerapiPasien(models.Model):
    _name = "terapi.pasien"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char("Nama Pasien")
    tgl_periksa = fields.Date("Tanggal Periksa")
    prosedur = fields.Char("Prosedur")
    obat = fields.Char("Obat")
    vaksinasi = fields.Char("Vaksinasi")
    perlakuan = fields.Char("Perlakuan")
    tes_lab = fields.Char("Test Lab")

    def name_get(self):
        result = []
        for mt in self:
            result.append((mt.id, "%s" % (mt.name or '')))
        return result

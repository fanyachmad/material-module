from odoo import api, fields, models
from odoo.exceptions import UserError, ValidationError

class DataPasien(models.Model):
    _name = "data.pasien"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char("Nama Lengkap", required=True)
    alamat = fields.Char("Alamat Lengkap")
    telp = fields.Char("No. Telp/Hp")
    riwayat_alergi = fields.Char("Rowayat Alergi")
    riwayat_penyakit = fields.Char("Riwayat Penyakit")
    nik = fields.Char("NIK")
    tgl_lahir = fields.Date("Tgl Lahir")
    jenis_kelamin = fields.Selection([('laki-laki', 'Laki-laki'), ('perempuan', 'Perempuan')], string="Jenis Kelamin")
    agama = fields.Selection([('islam', 'Islam'),
                              ('kristen', 'Kristen'),
                              ('katolik', 'Katolik'),
                              ('hindu', 'Hindu'),
                              ('budha', 'Budha')], string="Agama")
    pekerjaan = fields.Char("Pekerjaan")
    pasien_line_ids = fields.One2many('data.pasien.line', 'pasien_id', 'Pasien')
    check_in = fields.Date('Tanggal Check In')

    def _get_printed_report_name(self):
        self.ensure_one()
        return self.name

    def name_get(self):
        result = []
        for mt in self:
            result.append((mt.id, "%s" % (mt.name or '')))
        return result

class DataPasienLine(models.Model):
    _name = "data.pasien.line"
    _description = "Pasien Line"

    tgl = fields.Date("Tanggal Periksa")
    anamesis = fields.Many2one('pemeriksaan.pasien', string="Anemesis/Pemeriksaan")
    diagnosis = fields.Many2one('diagnosis.pasien', string="Diagnosis")
    terapi = fields.Many2one('terapi.pasien', string="Terapi")
    paraf = fields.Char("Paraf / Nama")
    pasien_id = fields.Many2one('data.pasien', "Pasien", ondelete='cascade', index=True)
from odoo import api, fields, models
from odoo.exceptions import UserError, ValidationError

class Diagnosis(models.Model):
    _name = "diagnosis.pasien"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char("Nama Pasien")
    tgl_periksa = fields.Date("Tanggal Periksa")
    diagnosis = fields.Char("Diagnosis")
    saran_medis = fields.Char("Saran Medis")

    def name_get(self):
        result = []
        for mt in self:
            result.append((mt.id, "%s" % (mt.name or '')))
        return result
from odoo import api, models, fields
from odoo.exceptions import ValidationError, UserError

class MaterialTest(models.Model):
    _name = 'material.test'
    _description = 'Material Register'

    material_code = fields.Char(string='Material Code')
    material_name = fields.Char(string='Material Name')
    material_type = fields.Selection([('fabric', 'Fabric'), ('jeans', 'Jeans'), ('cotton', 'Cotton')], string='Material Type')
    state = fields.Selection([('draft', 'Draft'), ('confirm', 'Confirmed'), ('cancel', 'Cancelled')], string='Status',
                             default='draft')
    material_buy_price = fields.Integer(string='Material Buy Price', default=100)
    supplier = fields.Many2one(comodel_name='supplier.name',
                               string='Supplier', required=True)

    def name_get(self):
        result = []
        for mt in self:
            result.append((mt.id, "%s" % (mt.material_name or '')))
        return result

    @api.onchange('material_buy_price')
    def buy_price(self):
        if self.material_buy_price < 100:
            raise UserError("Buy Price tidak boleh dibawah 100")

    def action_button_draft(self):
        if self.filtered(lambda inv: inv.state not in ('cancel')):
            raise UserError(('Plan must be on cancel state '))
        return self.write({'state': 'draft'})

    def action_button_confirm(self):
        if self.filtered(lambda inv: inv.state not in ('draft')):
            raise UserError(('Plan must be on draft state '))
        return self.write({'state': 'confirm'})

    def action_button_cancel(self):
        if self.filtered(lambda inv: inv.state not in ('confirm')):
            raise UserError(('Plan must be on confirmed state '))
        return self.write({'state': 'cancel'})

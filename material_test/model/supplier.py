from odoo import api, fields, models

class Supplier(models.Model):
    _name = 'supplier.name'
    _description = 'Supplier'

    supplier_name = fields.Char(string='Nama Supplier', required=True)

    def name_get(self):
        result = []
        for spl in self:
            result.append((spl.id, "%s" % (spl.supplier_name or '')))
        return result
from odoo.tests import common

class TestProject(common.TransactionCase):

    def test_project(self):
        supplier_project = self.env['supplier.name'].create({
            'supplier_name': 'Pasti Jaya'
        })

        material_project = self.env['material.test'].create({
            'material_name': 'Chino',
            'supplier': supplier_project.id,
            'material_code': '010',
            'material_type': 'cotton',
            'material_buy_price': 800
        })

        self.assertEqual(supplier_project.name, 'Pasti Jaya')
        self.assertEqual(material_project.name, 'Chino')

        self.assertEqual(material_project.supplier.id, supplier_project.id)

        print('Tes Berhasil')
import json
import requests

import werkzeug.wrappers
from odoo.http import request
from odoo import http, _, exceptions

class MaterialTestRestApi(http.Controller):

    #View semua material (api-akses : localhost:<port>/api/material_all/)
    @http.route('/api/material_all/', auth='public', csrf=False, type='http', methods=['GET'])
    def view_material_all(self, **params):
        material = http.request.env['material.test'].sudo().search([])
        dict_material = {}
        data_material = []
        for mat in material:
            dict_material = {
                'name': mat.material_name,
                'code': mat.material_code,
                'type': mat.material_type,
                'price': mat.material_buy_price,
            }
            data_material.append(dict_material)
        data = {
            'status': 200,
            'message': 'success',
            'response': data_material
        }
        try:
            return werkzeug.wrappers.Response(
                status=200,
                content_type='application/json; charset=utf-8',
                response=json.dumps(data)
            )
        except:
            return werkzeug.wrappers.Response(
                status=400,
                content_type='application/json; charset=utf-8',
                headers=[('Access-Control-Allow-Origin', '*')],
                response=json.dumps({
                    'error': 'Error',
                    'error_descrip': 'Error Description',
                })
            )

    # View material filter type (api-akses : localhost:<port>/api/material_all/?type=cotton)
    @http.route('/api/material/', auth='public' ,csrf=False, type='http', methods=['GET'])
    def view_material_type(self, **params):
        get_type = params.get("type")
        material = http.request.env['material.test'].sudo().search([('material_type', '=', get_type)])
        dict_material = {}
        data_material = []
        for mat in material:
            dict_material = {
                'name': mat.material_name,
                'code': mat.material_code,
                'type': mat.material_type,
                'price': mat.material_buy_price,
            }
            data_material.append(dict_material)
        data = {
            'status': 200,
            'message': 'success',
            'response': data_material
        }
        try:
            return werkzeug.wrappers.Response(
                status=200,
                content_type='application/json; charset=utf-8',
                response=json.dumps(data)
            )
        except:
            return werkzeug.wrappers.Response(
                status=400,
                content_type='application/json; charset=utf-8',
                headers=[('Access-Control-Allow-Origin', '*')],
                response=json.dumps({
                    'error': 'Error',
                    'error_descrip': 'Error Description',
                })
            )

    # Hapus material (api-akses : localhost:<port>/api/delete_material/?code=<value material_code>)
    @http.route('/api/delete_material/', auth='public', type='http', methods=['DELETE'], csrf=False)
    def del_material(self, **params):
        get_code = params.get("code")
        material = http.request.env['material.test'].sudo().search([('material_code', '=', get_code)])
        if material:
            material.unlink()
            try:
                return werkzeug.wrappers.Response(
                    status=200,
                    content_type='application/json; charset=utf-8',
                    response=json.dumps({
                        'message' : "Material Code %s successfully deleted" % (get_code), "delete": True
                    })
                )
            except:
                return werkzeug.wrappers.Response(
                    status=400,
                    content_type='application/json; charset=utf-8',
                    headers=[('Access-Control-Allow-Origin', '*')],
                    response=json.dumps({
                        'error': 'Error',
                        'error_descrip': 'Error Description',
                    })
                )

    # Update material filter type (api-akses : localhost:<port>/api/update_material/?code=<value material_code>)
    #add header key: name_material, value: misal=chino
    @http.route('/api/update_material/', auth='public', type='http', methods=['POST'], csrf=False)
    def updt_material(self, **kw):
        get_code = kw.get("code")
        get_name = kw.get("name_material")
        get_price = kw.get("price")


        material = http.request.env['material.test'].search([('material_code', '=', get_code)])
        # updt_mat = material.browse(get_code)
        if material:
            material.write({
                'material_name': http.request.params.get('name'),
            })
            try:
                return werkzeug.wrappers.Response(
                    status=200,
                    content_type='application/json; charset=utf-8',
                    response=json.dumps({
                        'message': "Material Code %s successfully updated" % (get_name), "update": True
                    })
                )
            except:
                return werkzeug.wrappers.Response(
                    status=400,
                    content_type='application/json; charset=utf-8',
                    headers=[('Access-Control-Allow-Origin', '*')],
                    response=json.dumps({
                        'error': 'Error',
                        'error_descrip': 'Error Description',
                    })
                )

